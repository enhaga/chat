# Synapse install 

We use the docker version. https://registry.hub.docker.com/r/matrixdotorg/synapse/#! has detailed instructions. To summarise:

1. `sudo docker pull matrixdotorg/synapse`


2. ```sudo docker run -it --rm --mount type=volume,src=synapse-data,dst=/data -e SYNAPSE_SERVER_NAME=my.matrix.host \
    -e SYNAPSE_REPORT_STATS=yes  matrixdotorg/synapse:latest generate```
	
3. Edit `sudo nano /var/lib/docker/volumes/synapse-data/_data/homeserver.yaml` to change configuration options.


# Run the server

```sudo docker run -d --name synapse --mount type=volume,src=synapse-data,dst=/data  -p 8008:8008  matrixdotorg/synapse:latest```
	
Confirm its running: `http://localhost:8008 `


# Usage

See https://matrix.org/docs/guides/client-server-api for more comprehensive guide.

Register `curl -XPOST -d '{"username":"nikolas", "password":"pass", "auth": {"type":"m.login.dummy"}}' "http://localhost:8008/_matrix/client/r0/register" `

Login `curl -XPOST -d '{"type":"m.login.password", "user":"nikolas", "password":"pass"}' "http://localhost:8008/_matrix/client/r0/login" `. This returns an access token which is used in subsequent communications.


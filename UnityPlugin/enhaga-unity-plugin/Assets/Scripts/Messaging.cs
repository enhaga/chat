using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class Messaging : MonoBehaviour
{
    public string Server =  "http://localhost:8008/_matrix/client/r0/login";
    public string Username;
    public string Password;

    private string reply;
    

    // Start is called before the first frame update
    void Start()
    {
	// Login
	string json = "{\"type\":\"m.login.password\", \"user\":\"nikolas\", \"password\":\"pass\"}";
        StartCoroutine(Post(Server, json, Login));
    }

    void Update()
    {
	// periodidcally get messages
    }

    IEnumerator Post(string url, string bodyJsonString, System.Action<string> done)
    {
        var request = new UnityWebRequest(url, "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(bodyJsonString);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        yield return request.SendWebRequest();

        Debug.Log("Status Code: " + request.responseCode);
        if (request.result == UnityWebRequest.Result.Success)
        {
            done(request.downloadHandler.text);
        }
    }

    private void Login(string data){
        Debug.Log(data);
	LoginReply lr = JsonUtility.FromJson<LoginReply>(data);
	Debug.Log(lr.access_token);
    }

    public void SendMessage(string message){
        Debug.Log(message);
    }

}

[System.Serializable]
public class LoginReply {
    public string user_id;
    public string access_token;
    public string device_id;
}
